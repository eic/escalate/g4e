#include "SingleSubdetectorConstruction.hh"

#include <G4SystemOfUnits.hh>
#include <G4PVPlacement.hh>
#include <G4VisAttributes.hh>
#include <G4NistManager.hh>
#include <G4Tubs.hh>

#include "Materials.hh"
#include "DetectorConfig.hh"
#include "StringHelpers.hh"
#include "InitializationContext.hh"




SingleSubdetectorConstruction::SingleSubdetectorConstruction(g4e::InitializationContext *initContext, DetectorConfig &config):
    fInitContext(initContext),
    fConfig(config),
    fMessenger(this, "/eic/subdetector/", "EIC Single subdetector construction messenger")
{
    using namespace g4e;

    fMaterials = new g4e::Materials();
    fMessenger.DeclareProperty("name", fSubdetectorName, "Sub-Detector name");

    // Transition Radiation Physics
    // >oO
//    initContext->PhysicsList->RegisterPhysics(ci_TRD.PhysicsConstructor);

    // Add stepping action that is executed on volume change
//    initContext->ActionInitialization->AddUserActionGenerator([initContext](){
//        auto action = new g4e::VolumeChangeSteppingAction(initContext->RootManager);
//        return static_cast<G4UserSteppingAction*>(action);
//    });
}

G4VPhysicalVolume *SingleSubdetectorConstruction::Construct() {
    using namespace fmt;

    //===================================================================================
    //==                    create a world                                            ==
    //===================================================================================

    // If this function is called, this means that there is no volume

    double SizeR = 300. * cm;
    double SizeZ = 50000. * cm;
    fWorldMaterial = fMaterials->GetMaterial("G4_Galactic");
    fWorldSolidVol = new G4Box("WorldSolid", SizeR, SizeR, SizeZ / 2.);
    fWorldLogicVol = new G4LogicalVolume(fWorldSolidVol, fWorldMaterial, "WorldLogic");
    fWorldPhysicalVolume = new G4PVPlacement(nullptr, G4ThreeVector(), "WorldPhys", fWorldLogicVol, nullptr, false, 0);
    fmt::print("Initializing WORLD. x:{:<10}m y:{:<10}m z:{:<10}m", SizeR * 2 / m, SizeR * 2 / m, SizeZ / m);

    auto name = g4e::ToLowerCopy(fSubdetectorName);
    if(name == "ci_drich") {
        return Construct_ci_DRICH();
    }
    if(name == "ir_beampipe" || name == "beampipe") {
        return Construct_Beampipe();
    }

    if(name == "ir_beamline" || name == "beamline") {
        return Construct_Beamline();
    }

    if(name == "test_emcal") {
        return Construct_test_EMCAL();
    }

    G4Exception("SingleSubdetectorConstruction::Construct",
                "InvalidSubdetectorName", FatalException,
                "Subdetector name doesn't correspond to any known sub-detector");

    return nullptr;
}


void SingleSubdetectorConstruction::ConstructSDandField() {
    G4VUserDetectorConstruction::ConstructSDandField();

    G4SDManager *sdManager = G4SDManager::GetSDMpointer();

    auto name = g4e::ToLowerCopy(fSubdetectorName);
    if(name == "test_emcal") {

        // Central electron calorimeter SD
        if (!fCe_EMCAL_SD.Get()) {
            auto ce_emcal_sd = new ce_EMCAL_SD("ce_EMCAL_SD", fInitContext->RootManager);
            fCe_EMCAL_SD.Put(ce_emcal_sd);
        }
        sdManager->AddNewDetector(fCe_EMCAL_SD.Get());

        test_EMCAL.ModuleLogic->SetSensitiveDetector(fCe_EMCAL_SD.Get());
    }
}

G4VPhysicalVolume *SingleSubdetectorConstruction::Construct_Beampipe() {
    /** The function constructs BeamPipe standalone */
    auto bpCadDirectory = fmt::format("{}/resources/ip6/beampipe/", fInitContext->Arguments->HomePath);
    ir_Beampipe.Construct(fConfig.ir_Beampipe, fWorldPhysicalVolume, bpCadDirectory);
    return fWorldPhysicalVolume;
}

G4VPhysicalVolume *SingleSubdetectorConstruction::Construct_Beamline() {
    return fWorldPhysicalVolume;
}

G4VPhysicalVolume *SingleSubdetectorConstruction::Construct_ci_DRICH() {
    ci_DRICH.Construct(fConfig.ci_DRICH, fWorldMaterial,  fWorldPhysicalVolume);
    ci_DRICH.ConstructDetectors();

    return fWorldPhysicalVolume;
}


G4VPhysicalVolume *SingleSubdetectorConstruction::Construct_test_EMCAL()
{
    test_EMCAL.Construct(fConfig.test_EMCAL, fWorldMaterial, fWorldPhysicalVolume);
    test_EMCAL.OuterVolumeVisAttr->SetColor(G4Color(0.3, 0.5, 0.9, 0.1));
    test_EMCAL.ConstructDetails(); // --- inner detector with Crystals

    return fWorldPhysicalVolume;
}