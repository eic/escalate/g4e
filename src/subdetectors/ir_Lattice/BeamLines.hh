#ifndef G4E_BEAMLINES_HH
#define G4E_BEAMLINES_HH

enum class BeamLines {
    IP6 = 0,
    IP8 = 1
};


#endif //G4E_BEAMLINES_HH
