#ifndef G4E_TEST_EMCAL_HH
#define G4E_TEST_EMCAL_HH


#include <G4PVDivision.hh>
#include "G4RotationMatrix.hh"
#include "G4Material.hh"
#include "G4Color.hh"
#include "G4VisAttributes.hh"
#include <spdlog/spdlog.h>
#include <InitializationContext.hh>

struct test_EMCAL_Config
{
    //double SizeZ;
    double PosZ = 20 * cm;

    //............... EMCAL Crystals modules ......................
    double Thickness = 20. * cm;
    double Width = 2. * cm;
    double Gap = 0.01 * mm;
    double InnerR = 0 * cm;
    double OuterR = 4 * cm;

    // Messenger to control initialization properties from geant config file

    inline test_EMCAL_Config() {
        static G4GenericMessenger *Messenger;

        // Create a global messenger that will be used
        if(!Messenger) {
            // Set geant options
            Messenger = new G4GenericMessenger(this, "/eic/test_EMCAL/");
            Messenger->DeclareProperty("thickness", Thickness, "Thikness (z direction dimention) of PWO crystals ");
            Messenger->DeclareProperty("width", Width, "Width (and higth) of each PWO crystal");
            Messenger->DeclareProperty("gap", Gap, "Gap between PWO crystals ");
            Messenger->DeclareProperty("innerRadius", InnerR, "Inner radius or beam hole for block");
            Messenger->DeclareProperty("outerRadius", OuterR, "Outer radius of test CALL");
            Messenger->DeclareProperty("ZPoz", OuterR, "Z position shift");
        }
    }
};

// Central region, electron end-cap EMCAL
class test_EMCAL_Design
{
public:

    test_EMCAL_Design()=default;

    inline void Construct(test_EMCAL_Config& cfg, G4Material *worldMaterial, G4VPhysicalVolume *motherVolume)
    {
        ConstructionConfig = cfg;
        spdlog::debug("Construct test_EMCAL");

        Solid = new G4Box("test_EMCAL_GVol_Solid", 2*cfg.OuterR+0.5*cm, 2*cfg.OuterR+0.5*cm, cfg.Thickness * 2. +1*mm);
        Logic = new G4LogicalVolume(Solid, worldMaterial, "test_EMCAL_GVol_Logic");
        OuterVolumeVisAttr = new G4VisAttributes(G4Color(0.3, 0.5, 0.9, 0.9));
        OuterVolumeVisAttr->SetLineWidth(1);
        OuterVolumeVisAttr->SetForceSolid(false);
        Logic->SetVisAttributes(OuterVolumeVisAttr);

        //   my_z= 0*cm;
        Phys = new G4PVPlacement(nullptr, G4ThreeVector(0, 0, cfg.PosZ), "test_EMCAL_GVol_Phys", Logic, motherVolume, false, 0);
    }

    /// CE EMCAL module Crystals
    inline void ConstructDetails()
    {
        auto cfg = ConstructionConfig;

        ModuleMaterial = fMat->GetMaterial("PbWO4");
        ModuleSolid = new G4Box("test_EMCAL_detPWO_Solid", cfg.Width * 0.5, cfg.Width * 0.5, cfg.Thickness * 0.5);
        ModuleLogic = new G4LogicalVolume(ModuleSolid, ModuleMaterial, "test_EMCAL_detPWO_Logic");

        ModuleVisAttr = new G4VisAttributes(G4Color(0.1, 1.0, 0.9, 0.5));
        ModuleVisAttr->SetLineWidth(1);
        ModuleVisAttr->SetForceSolid(true);
        ModuleLogic->SetVisAttributes(ModuleVisAttr);

        fmt::print("\nCE EMCAL PWO START\n");

        // CRYSTAL
        double diameter = 2 * cfg.OuterR;

        // How many towers do we have per row/column?
        // Add a gap + diameter as if we have N towers, we have N-1 gaps;
        int towersInRow = std::floor((diameter + cfg.Gap) / (cfg.Width + cfg.Gap));

        // Is it odd or even number of towersInRow
        double leftTowerPos, topTowerPos;
        if(towersInRow%2) {
            //             |
            //      [ ][ ][ ][ ][ ]
            //       ^     |
            int towersInHalfRow = std::floor(towersInRow/2.0);
            leftTowerPos = topTowerPos = -towersInHalfRow * (cfg.Width + cfg.Gap);
        } else {
            //               |
            //      [ ][ ][ ][ ][ ][ ]
            //       ^      |
            int towersInHalfRow = std::ceil(towersInRow/2.0);
            leftTowerPos = topTowerPos = -(towersInHalfRow - 0.5) * (cfg.Width + cfg.Gap);
        }

        fmt::print("\nCE EMCAL PWO SQUARE START\n");
        fmt::print("Thickness = {} cm;\n", cfg.Thickness / cm);
        fmt::print("Width     = {} cm;\n", cfg.Width / cm);
        fmt::print("Gap       = {} cm;\n", cfg.Gap / cm);
        fmt::print("InnerR    = {} cm;\n", cfg.InnerR / cm);
        fmt::print("OuterR    = {} cm;\n", cfg.OuterR / cm);
        fmt::print("PosZ      = {} cm;\n", cfg.PosZ / cm);
        fmt::print("Towers in Row/Col   = {};\n", towersInRow);
        fmt::print("Top left tower pos  = {:<10} {:<10} cm;\n", -leftTowerPos / cm, topTowerPos / cm);

        // fmt::print("#Towers info:\n");
        // fmt::print("#{:<5} {:<6} {:<3} {:<3} {:>10} {:>10}   {}\n", "idx",  "code", "col", "row", "x", "y", "name");
        int towerIndex = 0;
        for(int colIndex=0; colIndex < towersInRow; colIndex++) {
            for(int rowIndex=0; rowIndex < towersInRow; rowIndex++) {
                double x = leftTowerPos + colIndex * (cfg.Width + cfg.Gap);
                double y = topTowerPos + rowIndex * (cfg.Width + cfg.Gap);

                if ((std::abs(y) < cfg.OuterR && std::abs(x) < cfg.OuterR))
                    // gap &&(std::abs(y) > cfg.InnerR || std::abs(x) > cfg.InnerR))
                {
                    int code = 1000 * rowIndex + colIndex;
                    std::string name(fmt::format("test_EMCAL_pwo_phys_{}", code));
                    new G4PVPlacement(nullptr, G4ThreeVector(x, y, 1*mm), name, ModuleLogic, Phys, false, code);
                    // fmt::print(" {:<5} {:<6} {:<3} {:<3} {:>10.4f} {:>10.4f}   {}\n", towerIndex, code, colIndex, rowIndex, x / cm, y / cm, name);
                    towerIndex++;

                }
            }
        }
        fmt::print("Total PWO modules: {}\n", towerIndex);
        fmt::print("\nCE EMCAL PWO END\n");
    }


    G4Box *Solid;              // pointer to the solid
    G4LogicalVolume *Logic;     // pointer to the logical
    G4VPhysicalVolume *Phys;    // pointer to the physical

    G4VisAttributes *OuterVolumeVisAttr;

    /// Parameters that was used in the moment of construction
    test_EMCAL_Config ConstructionConfig;

    //--- crystals
    G4Material *ModuleMaterial;
    G4VisAttributes *ModuleVisAttr;
    G4Box *ModuleSolid;
    G4LogicalVolume *ModuleLogic;

    g4e::Materials *fMat;
private:
};

#endif //G4E_CE_EMCAL_HH
